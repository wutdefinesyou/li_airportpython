# Handle an input file, read the data inside and put them in a new list
#

import airplane

class Reader:

    # initialize with a queue
    def __init__(self):
        self.queue = list()

    # read from a file and load airplane information in the queue
    def read(self, file):
        with open(file, "r") as f:
            data = f.readlines()

            for line in data:
                words = line.split(',')
                myAirplane = airplane.Airplane(words[0], words[1], words[2], words[3])
                self.queue.append(myAirplane)
