# priorityQueue.py
# A priority queue for placing airplanes
#

import airplane

class PriorityQueue:

	def __init__(self):
		# current time slot for the airport
		self.time = 0
		# initialize a queue
		self.queue = list()

	def insert(self, airplane):
		# if the queue is empty, add the first airplane
		if len(self.queue) == 0:
			self.queue.append(airplane)
			self.queue[0].schedStart = self.queue[0].reqStart
			self.queue[0].schedEnd = self.queue[0].reqStart + self.queue[0].reqDur - 1
		# add the plane into the queue according to its subminssion time and requested start time
		else:
			# compare the new airplane to every item in the queue
			for x in range(0, len(self.queue)):
				# insert the ariplane before if its requested time is smaller
				if airplane.reqStart <= self.queue[x].reqStart:
					# if it is the case that the requested time is he same, check who got smaller submission time
					if (airplane.reqStart == self.queue[x].reqStart) and (airplane.subTime >= self.queue[x].subTime):
						self.queue.insert(x+1, airplane)
					else:
						self.queue.insert(x, airplane)
					# schedule actual take off time. If the new inserted airplane has overlap in its requeted
					# start time with the last airplane's scheduled ending time, push it off until the last
					# airplane took off. Otherwise it is good.
					for y in range(x, len(self.queue)):
						if self.queue[y].reqStart <= self.queue[y-1].schedEnd:
							self.queue[y].schedStart = self.queue[y-1].schedEnd + 1
						else:
							self.queue[y].schedStart = self.queue[y].reqStart
						self.queue[y].schedEnd = self.queue[y].schedStart + self.queue[y].reqDur - 1
					return True
				# if the target airplane has larger requested time
				else:
					# if it is the largest among all airplanes in the queue, insert it at the last spot
					if x == (len(self.queue) - 1):
						self.queue.insert(x+1, airplane)
						if self.queue[x+1].reqStart <= self.queue[x].schedEnd:
							self.queue[x+1].schedStart = self.queue[x].schedEnd + 1
						else:
							self.queue[x+1].schedStart = self.queue[x+1].reqStart
						self.queue[x+1].schedEnd = self.queue[x+1].schedStart + self.queue[x+1].reqDur - 1
					# continue to compare to the next airplane in the queue
					else:
						continue

	# When an airplane took off
	def remove(self):
		return self.queue.pop(0)

	# Time increased by 1
	def timeIncrement(self):
		self.time = self.time + 1

	# Print out the take off times
	def show(self):
		for x in self.queue:
			print (str(x.ID) + " (" + str(x.getSchedStart()) + "-" + str(x.getSchedEnd()) + ")")
