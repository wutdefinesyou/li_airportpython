# airplane.py

class Airplane:
    """An airplane structure"""

    def __init__(self):
        self.ID = ""
        self.subTime = 0
        self.reqStart = 0
        self.reqDur = 0
        self.schedStart = 0
        self.schedEnd = 0

    def __init__(self, ID, subTime, reqStart, reqDur):
        self.ID = ID
        self.subTime = int(subTime)
        self.reqStart = int(reqStart)
        self.reqDur = int(reqDur)
        self.schedStart = 0
        self.schedEnd = 0

    def getSchedStart(self):
        return self.schedStart

    def getSchedEnd(self):
        return self.schedEnd
