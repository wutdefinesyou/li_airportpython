# Main function
#

import airplane
import priorityQueue
import reader

def main():
    myRawSlot = reader.Reader()
    myRawSlot.read("sampleList.txt")
    mySlot = priorityQueue.PriorityQueue()
    for x in myRawSlot.queue:
        mySlot.insert(x)

    mySlot.show()

if __name__ == "__main__":
    main()
