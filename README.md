-Overview

This is an airport take-off time slot simulation in Python.

-Author

Zishuo Li

-How to compile

No compiler needed. Uses Python interpreter.

-File listing

.gitignore; SDD.docx; SDD.pdf; airplane.py

-How to run

Python 3
